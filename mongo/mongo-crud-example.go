package main

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	DBName     = "demo"
	COLLECTION = "trainers"
	URI        = "mongodb://localhost:27017"
)

type Trainer struct {
	Name string
	Age  int
	City string
}

func main() {
	ctx := context.Background()
	client := getMongoClient(ctx)
	collection := client.Database(DBName).Collection(COLLECTION)

	data := sampleData()
	insertSingleDocument(collection, ctx, data[0])
	insertManyDocuments(collection, ctx, []interface{}{data[1], data[2]})
	updateDocument(collection, ctx)
	findSingleDocument(collection, ctx)
	findManyRecords(collection, ctx)
	deleteManyDocuments(collection, ctx)

	err := client.Disconnect(ctx)
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Connection to MongoDB closed.")
	}
}

func deleteManyDocuments(collection *mongo.Collection, ctx context.Context) {
	deleteResult, _ := collection.DeleteMany(ctx, bson.D{{}})
	fmt.Printf("Deleted %v documents in the trainers collection\n", deleteResult.DeletedCount)
}

func findManyRecords(collection *mongo.Collection, ctx context.Context) {
	var results []*Trainer
	findOptions := options.Find().SetLimit(2)
	cur, _ := collection.Find(ctx, bson.D{{}}, findOptions)
	for cur.Next(ctx) {
		var elem Trainer
		cur.Decode(&elem)
		results = append(results, &elem)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	cur.Close(ctx)
	fmt.Printf("Found multiple documents (array of pointers): %+v\n", results)
}

func findSingleDocument(collection *mongo.Collection, ctx context.Context) {
	var result Trainer
	collection.FindOne(ctx, filterByName()).Decode(&result)
	fmt.Printf("Found a single document: %+v\n", result)
}

func updateDocument(collection *mongo.Collection, ctx context.Context) {
	update := bson.D{
		{"$inc", bson.D{
			{"age", 1},
		}},
	}
	updateResult, _ := collection.UpdateOne(ctx, filterByName(), update)
	fmt.Printf("Matched %v documents and updated %v documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)
}

func filterByName() bson.D {
	return bson.D{{"name", "Ash"}}
}

func insertManyDocuments(collection *mongo.Collection, ctx context.Context, trainers []interface{}) {
	insertManyResult, _ := collection.InsertMany(ctx, trainers)
	fmt.Println("Inserted multiple documents: ", insertManyResult.InsertedIDs)
}

func insertSingleDocument(collection *mongo.Collection, ctx context.Context, data Trainer) {
	insertResult, _ := collection.InsertOne(ctx, data)
	fmt.Println("Inserted a single document: ", insertResult.InsertedID)
}

func sampleData() []Trainer {
	ash := Trainer{"Ash", 10, "Pallet Town"}
	misty := Trainer{"Misty", 10, "Cerulean City"}
	brock := Trainer{"Brock", 15, "Pewter City"}
	return []Trainer{ash, misty, brock}
}

func getMongoClient(ctx context.Context) *mongo.Client {
	clientOptions := options.Client().ApplyURI(URI)
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")
	return client
}
